let reduce = require('./reduce')

const items = [1, 2, 3, 4, 5, 5]; 
// The callback should add up all numbers in array and return a single number

const sumCallback = (accumulator, value) => accumulator + value;
 console.log('Sum: ', reduce(items, sumCallback, 0));